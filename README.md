HTTP Factories and HTTP Message
===============================

Package integrates laminas/laminas-diactoros with F2 service container.


Services added to F2::container():

* `Psr\Http\Message\RequestFactoryInterface`
* `Psr\Http\Message\ResponseFactoryInterface`
* `Psr\Http\Message\ServerRequestFactoryInterface`
* `Psr\Http\Message\StreamFactoryInterface`
* `Psr\Http\Message\UploadedFileFactoryInterface`
* `Psr\Http\Message\UriFactoryInterface`


Easy Access Functions added:

* `F2::createRequest`
* `F2::createResponse`
* `F2::createServerRequest`
* `F2::createStream`
* `F2::createStreamFromResource`
* `F2::createUploadedFile`
* `F2::createUri`


