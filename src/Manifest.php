<?php
namespace F2\Http;

use Psr\Http\Message\RequestFactoryInterface;
use Laminas\Diactoros\RequestFactory;

use Psr\Http\Message\ResponseFactoryInterface;
use Laminas\Diactoros\ResponseFactory;

use Psr\Http\Message\ServerRequestFactoryInterface;
use Laminas\Diactoros\ServerRequestFactory;

use Psr\Http\Message\StreamFactoryInterface;
use Laminas\Diactoros\StreamFactory;

use Psr\Http\Message\UploadedFileFactoryInterface;
use Laminas\Diactoros\UploadedFileFactory;

use Psr\Http\Message\UriFactoryInterface;
use Laminas\Diactoros\UriFactory;


class Manifest extends \F2\Common\AbstractManifest {

    public function getDependencies(): iterable {
        return [
            \F2\Container\Manifest::class,
            \F2\Common\Manifest::class,
        ];
    }
    /**
     * These are essential methods that should be added to the F2 class
     */
    public function getF2Methods(): iterable {
        yield "createRequest"           => [ HttpFactory::class, 'createRequest' ];
        yield "createResponse"          => [ HttpFactory::class, 'createResponse' ];
        yield "createServerRequest"     => [ HttpFactory::class, "createServerRequest" ];
        yield "createStream"            => [ HttpFactory::class, 'createStream' ];
        yield "createStreamFromFile"    => [ HttpFactory::class, 'createStreamFromFile' ];
        yield "createStreamFromResource" => [ HttpFactory::class, 'createStreamFromResource' ];
        yield "createUploadedFile"      => [ HttpFactory::class, "createUploadedFile" ];
        yield "createUri"               => [ HttpFactory::class, 'createUri' ];
    }

    public function registerConfigDefaults(array $config): array {
        $config['container/services'][] = [ 'class', RequestFactory::class, RequestFactoryInterface::class, true, ];
        $config['container/services'][] = [ 'class', ResponseFactory::class, ResponseFactoryInterface::class, true, ];
        $config['container/services'][] = [ 'class', ServerRequestFactory::class, ServerRequestFactoryInterface::class, true, ];
        $config['container/services'][] = [ 'class', StreamFactory::class, StreamFactoryInterface::class, true, ];
        $config['container/services'][] = [ 'class', UploadedFileFactory::class, UploadedFileFactoryInterface::class, true, ];
        $config['container/services'][] = [ 'class', UriFactory::class, UriFactoryInterface::class, true, ];
        return $config;
    }

}
